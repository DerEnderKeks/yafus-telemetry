import maxmind, {CountryResponse} from 'maxmind'
import express, {Request, Response} from 'express'
import rateLimit from 'express-rate-limit'
import sqlite3 from "sqlite3"
import {open} from "sqlite"
import {v4 as uuidv4} from 'uuid'
import path from 'path';
import {GeoIPDBUpdater} from './geoip.js';

const geoDBPath = './data/geolite';

(async () => {
    const db = await open({
        filename: './data/telemetry.db',
        driver: sqlite3.cached.Database
    })


    await db.exec(`
        CREATE TABLE IF NOT EXISTS "instances"
        (
            "id"        TEXT     NOT NULL PRIMARY KEY,
            "country"   TEXT,
            "users"     INTEGER,
            "domains"   INTEGER,
            "files"     INTEGER,
            "updatedAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        );
    `)

    const app = express()

    const port = 3000

    app.set('trust proxy', ['loopback', 'linklocal', 'uniquelocal'])
    app.use(express.json())

    app.get('/health', (req, res) => {
        res.json({status: 'ok'})
    })

    // route to get a summary of all instances that were active withing the last 2 days
    app.get('/summary', async (req, res) => {
        try {
            let countryCount = await db.all<any[]>(`
                SELECT country, count(country) AS count
                FROM instances
                WHERE country NOT NULL
                  AND updatedAt >= DATETIME('now', '-2 day')
                GROUP BY country;`)
            countryCount = Object.fromEntries(countryCount.map((c: any) => {
                return [c.country, c.count]
            }))
            const sums = await db.get(`
                SELECT sum(files) as files, sum(users) as users, sum(domains) as domains
                FROM instances
                WHERE updatedAt >= DATETIME('now', '-2 day');`)
            const avgs = await db.get(`
                SELECT cast(avg(files) as integer)   as files,
                       cast(avg(users) as integer)   as users,
                       cast(avg(domains) as integer) as domains
                FROM instances
                WHERE updatedAt >= DATETIME('now', '-2 day');`)
            res.json({countries: countryCount, total: sums, average: avgs})
        } catch (err) {
            res.status(500).json({status: 'failed', message: 'Internal error'})
        }
    })

    interface ReportData {
        users: number
        domains: number
        files: number
        country: string
    }

    function isReportData(data: ReportData): data is ReportData {
        let result = true;
        [data.files, data.domains, data.users].forEach(el => {
            if (!Number.isInteger(el)) result = false
        })
        return result
    }

    function sendError(res: Response, status: Number, message: String) {
        res.header(status)
        res.json({status: 'failed', message: message})
    }

    async function getCountryCode(ip: string): Promise<string | null> {
        try {
            let lookup = await maxmind.open<CountryResponse>(path.join(geoDBPath, 'GeoLite2-Country.mmdb'));
            let country = lookup.get(ip);
            if (country && country.country) return country.country.iso_code
        } catch (err) {
            console.error('Failed to get country code for ip: ' + err)
        }
        return null
    }

    function limiterHandler(req: Request, res: Response) {
        console.log(`Rate-limited ip: ${req.ip}`)
        res.status(429).json({status: 'failed', message: 'Too many requests'})
    }

    const reportLimiter = rateLimit({
        windowMs: 5 * 60 * 1000,
        max: 5,
        handler: limiterHandler
    });

    // route to update data for one instance
    app.post('/report/:uuid', reportLimiter, async (req, res) => {
        const uuid = req.params['uuid']
        if (!uuid || uuid.length < 36) return sendError(res, 400, 'UUID invalid')
        const data: ReportData = req.body
        let instance = await db.get(`SELECT id
                                     FROM instances
                                     WHERE id = ?`, uuid)
        if (!instance || !isReportData(data)) return sendError(res, 400, 'Invalid data')
        res.status(202).json({status: 'accepted'})
        const country = await getCountryCode(req.ip)
        let newData: any = {
            $i: instance.id,
            $u: data.users,
            $d: data.domains,
            $f: data.files
        }
        if (country) newData.$c = country
        db.run(`UPDATE instances
                SET updatedAt = CURRENT_TIMESTAMP,
                    users   = $u,
                    domains = $d,
                    files   = $f${country ? ', country = $c' : ''}
                WHERE id = $i;`, newData).then(() => {
            console.log(`Updated instance: ${instance.id}`)
        }).catch((err) => {
            console.error(`Failed to update instance: ${instance!.id}`, err)
        })
    })

    const registerLimiter = rateLimit({
        windowMs: 60 * 60 * 1000,
        max: 5,
        handler: limiterHandler
    });

    // route to register a new instance and get the UUID of it
    app.post('/register', registerLimiter, async (req, res) => {
        try {
            const newRow = await db.run(`
                INSERT INTO instances (id)
                VALUES (?);`, uuidv4())
            const instance = await db.get(`
                SELECT id
                FROM instances
                WHERE ROWID = ?`, newRow.lastID)
            res.status(201).json({status: 'success', uuid: instance.id})
            console.log(`Instance registered: ${instance.id}`)
        } catch (err) {
            res.status(500).json({status: 'failed', message: 'Internal error'})
            console.error('Failed to register instance: ', err)
        }
    });

    (new GeoIPDBUpdater(geoDBPath)).scheduleUpdates()

    app.listen(port, () => {
        console.log(`Listening on :${port}`)
    })
})()