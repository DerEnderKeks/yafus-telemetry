import https from 'https'
import * as fs from 'fs'
import path from 'path'
import * as crypto from 'crypto';
import * as zlib from 'zlib';
import tar from 'tar'

// loosely based on https://github.com/GitSquared/node-geolite2-redist/blob/master/scripts/download-helper.js
export class GeoIPDBUpdater {
    dbPath: string
    tmpPath: string

    dbURL = 'https://raw.githubusercontent.com/GitSquared/node-geolite2-redist/master/redist/GeoLite2-Country.tar.gz'
    sumURL = 'https://raw.githubusercontent.com/GitSquared/node-geolite2-redist/master/redist/GeoLite2-Country.mmdb.sha384'

    constructor(dbPath: string) {
        this.dbPath = path.join(dbPath, 'GeoLite2-Country.mmdb')
        this.tmpPath = this.dbPath + '.tmp'
    }

    // boolean indicates whether a new version is available
    checkForUpdates(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            if (!fs.existsSync(this.dbPath)) return resolve(true)
            let newChecksum = ''
            https.get(this.sumURL, res => {
                res.on('data', chunk => {
                    newChecksum += chunk.toString()
                }).on('end', () => {
                    newChecksum = newChecksum.trim()
                    if (!newChecksum || newChecksum.length !== 96) return reject(new Error('Failed to download checksum, got: ' + newChecksum))
                    fs.readFile(this.dbPath, (err, buffer) => {
                        if (err) return reject(err)
                        const checksum = crypto.createHash('sha384').update(buffer).digest('hex');
                        if (checksum === newChecksum) {
                            resolve(false)
                        } else {
                            resolve(true)
                        }
                    })
                }).on('error', e => {
                    reject(e)
                })
            })
        })
    }

    downloadUpdates() {
        return new Promise<void>((resolve, reject) => {
            https.get(this.dbURL, res => {
                res.pipe(zlib.createGunzip({})).pipe(tar.t())
                    .on('entry', entry => {
                        if (!entry.path.endsWith('.mmdb')) return;
                        let outStream
                        try {
                            fs.mkdirSync(path.dirname(this.dbPath), {recursive: true})
                            outStream = fs.createWriteStream(this.tmpPath)
                            outStream.on('error', reject)
                            entry.on('error', reject)
                            entry.pipe(outStream)
                        } catch (e) {
                            reject(e)
                        }
                    }).on('error', reject)
                    .on('finish', () => {
                        fs.rename(this.tmpPath, this.dbPath, () => {
                            resolve()
                        })
                    })
            })
        })
    }

    scheduleUpdates() {
        this.checkForUpdates().then(res => {
            if (res) this.downloadUpdates()
                .then(() => {
                    console.log('Updated GeoIP DB')
                })
                .catch(e => {
                    console.error('Failed to download GeoIP DB update: ', e)
                })
        }).catch(err => {
            console.error('GeoIP DB update check failed: ', err)
        }).finally(() => {
            setTimeout(() => {
                this.scheduleUpdates()
            }, 24 * 60 * 60 * 1000)
        })
    }
}
