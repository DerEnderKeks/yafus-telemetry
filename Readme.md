# Yafus Telemetry

This is a *very* basic telemetry server to collect data for [Yafus](https://gitlab.com/DerEnderKeks/yafus).

Only anonymous data is stored, specifically the country, file count, user count and domain count.

It has the following endpoints:

- `GET` `/summary` - Get the data summary, only active instances (withing the last 2 days) are evaluated
- `POST` `/register` - Register a new instance and get a uuid
- `POST` `/report/<uuid>` - Report data for a UUID

The `POST` endpoints are heavily rate limited, to prevent people from doing silly things.

While this server is very specific to Yafus, you could easily adopt it for other things.