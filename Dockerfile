FROM node:16-alpine AS builder

WORKDIR /app
COPY package.json yarn.lock ./
RUN npm install -g npm && yarn install
COPY . .
RUN yarn run build
RUN ls -la

FROM node:16-alpine
LABEL maintainer="DerEnderKeks"

WORKDIR /app

RUN apk add --update --no-cache \
    curl \
    tzdata

RUN mkdir data && \
    chown -R node:node data

USER node
COPY --from=builder /app /app

VOLUME /app/data
HEALTHCHECK CMD curl -Isf http://localhost:3000/health || exit 1
ENV NODE_ENV=production
ENTRYPOINT ["yarn", "start"]
